<?php

namespace Drupal\a12s_seo;

/**
 * The "SEO Manager" service.
 */
class SeoManager {

  /**
   * Get the list of all "rel" attributes to remove.
   *
   * By default, Drupal uses all entity "links" to generate the HTML links
   * tags in the header.
   * But some of them are useless, some leads to 403 errors, some must be
   * restricted to authenticated users, ...
   *
   * So this method will return a hardcoded list of useless rel attributes.
   *
   * @see https://www.flocondetoile.fr/blog/maitriser-les-entetes-de-drupal-8-et-son-seo
   * @see https://www.drupal.org/project/drupal/issues/2821635
   * @see https://www.drupal.org/project/drupal/issues/2406533
   *
   * @return array
   *   The full list.
   */
  public function getUselessRelAttributes(): array {
    return [
      'delete-form',
      'delete-multiple-form',
      'edit-form',
      'version-history',
      'revision',
      'create',
    ];
  }

}
