<?php

namespace Drupal\a12s_layout\Plugin\A12sLayoutDisplayOptionsSet;

use Drupal\a12s_layout\DisplayOptions\DisplayOptionsSetPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of Display Options Set for Grid settings.
 *
 * @A12sLayoutDisplayOptionsSet(
 *   id = "grid_gap",
 *   label = @Translation("Grid gap"),
 *   description = @Translation("Define the gap between the columns."),
 *   category = @Translation("Size and spacing"),
 *   applies_to = {"layout"},
 *   target_template = "layout"
 * )
 *
 * @noinspection AnnotationMissingUseInspection
 */
class GridGap extends DisplayOptionsSetPluginBase {

  /**
   * {@inheritDoc}
   */
  public function defaultValues(): array {
    return [
      'gap' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function preprocessVariables(array &$variables, array $configuration = []): void {
    parent::preprocessVariables($variables, $configuration);
    $variables['#attached']['library'][] = 'a12s_layout/gap';
    $this->addClasses($variables['attributes'], !empty($configuration['gap']) ? $configuration['gap'] : 'gap');
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $formState, array $values = [], array $parents = []): array {
    $form['gap'] = [
      '#type' => 'select',
      '#title' => $this->t('Gap'),
      '#empty_option' => $this->t('- Default -'),
      '#default_value' => $values['gap'] ?? '',
      // @todo Define those options in global settings form.
      '#options' => [
        'gap-0' => $this->t('No gap'),
      ],
    ];

    return $form;
  }

}
