<?php

namespace Drupal\a12s_layout\DisplayOptions;

use Drupal\a12s_core\FormHelperTrait;
use Drupal\a12s_core\ThemeHelperTrait;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Display Options Set plugins.
 */
abstract class DisplayOptionsSetPluginBase extends PluginBase implements DisplayOptionsSetInterface {

  use FormHelperTrait;
  use StringTranslationTrait;
  use ThemeHelperTrait;

  /**
   * The stored global configuration for this plugin.
   *
   * @var mixed
   */
  protected mixed $globalConfiguration;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $config = $configFactory->get('a12s_layout.display_options')->get($this->getMachineName()) ?? [];
    $this->globalConfiguration = $this->mergeConfigWithDefaults($config);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): DisplayOptionsSetInterface {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultValues(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function mergeConfigWithDefaults(array $config = []): array {
    $default = $this->defaultValues();
    // Keep only keys which are present in the default values.
    $values = array_intersect_key($config, $default);
    return $values + $default;
  }

  /**
   * {@inheritDoc}
   */
  public function appliesToTemplate(string $name): bool {
    $definition = $this->getPluginDefinition();
    return !empty($definition['target_template']) && $name === $definition['target_template'];
  }

  /**
   * {@inheritDoc}
   */
  public function preprocessVariables(array &$variables, array $configuration = []): void {}

  /**
   * {@inheritDoc}
   */
  public function globalSettingsForm(array &$form, FormStateInterface $formState, array $config = []): void {}

  /**
   * {@inheritDoc}
   */
  public function validateGlobalSettingsForm(array $form, FormStateInterface $formState): void {}

  /**
   * {@inheritDoc}
   */
  public function submitGlobalSettingsForm(array $form, FormStateInterface $formState): void {
    // @todo are we sure that json_encode keep the same order? Or should we
    //   sort the array first?
    $toString = fn($a) => json_encode($a);
    $fromString = fn($a) => json_decode($a, TRUE);
    $default = $this->defaultValues();
    $flattenDefault = array_map($toString, $default);

    // Keep only keys which are present in the default values.
    $values = array_intersect_key($formState->getValues(), $default);
    $flattenValues = array_map($toString, $values);
    // Keep only values that differs from defaults.
    $values = array_diff_assoc($flattenValues, $flattenDefault);
    $formState->setValues(array_map($fromString, $values));
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState): void {}

  /**
   * {@inheritDoc}
   */
  public function getMachineName(): string {
    return strtr($this->getPluginId(), [':' => '_']);
  }

}
