<?php

namespace Drupal\a12s_layout\DisplayOptions;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Utility\Error;

/**
 * Display Template plugin manager.
 */
class DisplayTemplatePluginManager extends DefaultPluginManager implements CategorizingPluginManagerInterface {

  use CategorizingPluginManagerTrait;

  /**
   * Constructs "a12s layout display options" PluginManager object.
   *
   * {@inheritDoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/A12sLayoutDisplayTemplate',
      $namespaces,
      $module_handler,
      'Drupal\a12s_layout\DisplayOptions\DisplayTemplatePluginInterface',
      'Drupal\a12s_layout\Annotation\A12sLayoutDisplayTemplate'
    );
    $this->alterInfo('a12s_layout_display_template_info');
    $this->setCacheBackend($cache_backend, 'a12s_layout_display_template_plugins');
  }

  /**
   * {@inheritDoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);
    $this->processDefinitionCategory($definition);
  }

  /**
   * {@inheritDoc}
   */
  protected function processDefinitionCategory(&$definition): void {
    // Ensure that every plugin has a category.
    if (empty($definition['category'])) {
      // Default to the label.
      $definition['category'] = $definition['label'];
    }
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\a12s_layout\DisplayOptions\DisplayTemplatePluginInterface
   *   A fully configured plugin instance.
   */
  public function createInstance($plugin_id, array $configuration = []): DisplayTemplatePluginInterface {
    $instance = parent::createInstance($plugin_id, $configuration);

    if ($instance instanceof DisplayTemplatePluginInterface) {
      return $instance;
    }

    throw new PluginException(sprintf('The plugin (%s) did not implement the expected interface "%s".', $plugin_id, DisplayTemplatePluginInterface::class));
  }

  /**
   * Preprocess variables for a specific template.
   *
   * @param string $pluginId
   *   The Display Template plugin ID.
   * @param string $name
   *   The template/theme name.
   * @param array $displayOptions
   *   The stored display options for the given template.
   * @param array $variables
   *   The variables to preprocess.
   */
  public static function preprocessVariables(string $pluginId, string $name, array $displayOptions, array &$variables): void {
    /** @var \Drupal\a12s_layout\DisplayOptions\DisplayTemplatePluginManager $thDisplayTemplateManager */
    $thDisplayTemplateManager = \Drupal::service('plugin.manager.a12s_layout_display_template');

    try {
      if ($layoutDisplayTemplate = $thDisplayTemplateManager->createInstance($pluginId)) {
        $layoutDisplayTemplate->executeTemplatePreprocessing($name, $displayOptions, $variables);
      }
    }
    catch (PluginException $e) {
      Error::logException(\Drupal::logger('a12s_layout'), $e);
    }
  }

}
