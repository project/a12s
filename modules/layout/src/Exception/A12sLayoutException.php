<?php

namespace Drupal\a12s_layout\Exception;

/**
 * Exception related to A12s Layout.
 */
class A12sLayoutException extends \Exception {}
