# A12S LAYOUT

## REQUIREMENTS

This module requires at least PHP 8.0.

## INSTALLATION

Install this module as any other Drupal module, see the documentation on
[Drupal.org](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).
