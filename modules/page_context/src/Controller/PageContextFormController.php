<?php

namespace Drupal\a12s_page_context\Controller;

use Drupal\a12s_page_context\PageContextManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller for "Page Context Form" entities.
 */
class PageContextFormController extends ControllerBase {

  /**
   * Construct a new PageContextFormController instance.
   *
   * @param \Drupal\a12s_page_context\PageContextManagerInterface $pageContextManager
   *   The Page Context Form manager.
   */
  public function __construct(protected PageContextManagerInterface $pageContextManager) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('a12s_page_context.manager')
    );
  }

  /**
   * Provides the listing page for any entity type.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function pathsOverview(): array {
    $table = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Key'),
        $this->t('Paths'),
        $this->t('Negate condition'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('There is currently no paths with custom values.'),
    ];

    foreach ($this->pageContextManager->getPaths() as $value) {
      $row = [];
      $row[] = $value['key'];
      $row[] = Markup::create(!empty($value['settings']['pages']) ? preg_replace('/[\r\n]+/', '<br />', $value['settings']['pages']) : '');
      $row[] = !empty($value['settings']['negate']) ? $this->t('Yes') : $this->t('No');
      $row[] = [
        'data' => [
          '#type' => 'dropbutton',
          '#dropbutton_type' => 'small',
          '#links' => [
            'edit' => [
              'title' => $this->t('Edit'),
              'url' => Url::fromRoute('entity.a12s_page_context_form.path_edit_form', [
                'path_key' => $value['key'],
              ]),
            ],
            'delete' => [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.a12s_page_context_form.path_delete_form', [
                'path_key' => $value['key'],
              ]),
            ],
          ],
        ],

      ];
      $table['#rows'][] = $row;
    }

    return $table;
  }

}
