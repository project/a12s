<?php

namespace Drupal\a12s_page_context;

/**
 * Represents a record from the database.
 */
class Record {

  /**
   * The record ID.
   *
   * @var int
   */
  protected int $id;

  /**
   * The ID of the related "Page Context Form" entity.
   *
   * @var string
   */
  protected string $config_id;

  /**
   * The related plugin ID.
   *
   * @var string
   */
  protected string $plugin_id;

  /**
   * The storage key.
   *
   * @var string
   */
  protected string $key;

  /**
   * The deserialized data.
   *
   * @var array
   */
  protected array $deserializedData = [];

  /**
   * The deserialized settings.
   *
   * @var array
   */
  protected array $deserializedSettings = [];

  /**
   * This setter takes care of deserializing stored values.
   *
   * @param string $name
   *   THe property name.
   * @param mixed $value
   *   The value to set.
   */
  public function __set(string $name, mixed $value): void {
    if (in_array($name, ['data', 'settings'])) {
      $this->{'deserialized' . ucfirst($name)} = !empty($value) ? unserialize($value) : [];
    }
  }

  /**
   * Get the record ID.
   *
   * @return int
   *   The record ID.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Get the page context form ID.
   *
   * @return string
   *   The entity ID.
   */
  public function getConfigId(): string {
    return $this->config_id;
  }

  /**
   * Get the plugin ID.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId(): string {
    return $this->plugin_id;
  }

  /**
   * Get the storage key.
   *
   * @return string
   *   The storage key.
   */
  public function getKey(): string {
    return $this->key;
  }

  /**
   * Get the record data.
   *
   * @return array
   *   The deserialized data.
   */
  public function getData(): array {
    return $this->deserializedData;
  }

  /**
   * Get the record settings.
   *
   * @return array
   *   The deserialized settings.
   */
  public function getSettings(): array {
    return $this->deserializedSettings;
  }

}
