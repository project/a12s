<?php

namespace Drupal\a12s_page_context;

use Drupal\a12s_core\EntityHelperInterface;
use Drupal\a12s_page_context\Entity\PageContextForm;
use Drupal\Core\Database\Connection;

/**
 * Manager for the "page context form" related features.
 */
class PageContextManager implements PageContextManagerInterface {

  /**
   * Constructs a new \Drupal\Core\Menu\MenuTreeStorage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Database connection to use for reading and writing configuration data.
   * @param \Drupal\a12s_core\EntityHelperInterface $entityHelper
   *   The Entity Helper service.
   */
  public function __construct(
    protected Connection $connection,
    protected EntityHelperInterface $entityHelper
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getPaths(): array {
    $results = [];
    $query = $this->connection->select('a12s_page_context_record', NULL, ['fetch' => \PDO::FETCH_ASSOC])
      ->fields('a12s_page_context_record')
      ->condition('plugin_id', 'path')
      ->groupBy('key');

    foreach ($query->execute() as $item) {
      $item['data'] = $item['data'] ? unserialize($item['data']) : [];
      $item['settings'] = $item['settings'] ? unserialize($item['settings']) : [];
      $results[$item['id']] = $item;
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function getData(string $configId, array $context = []): array {
    return PageContextForm::load($configId)?->getData($context) ?? [];
  }

}
