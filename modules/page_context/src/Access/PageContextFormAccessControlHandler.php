<?php

namespace Drupal\a12s_page_context\Access;

use Drupal\a12s_page_context\Entity\PageContextFormInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an entity access control handler for "Page context form".
 */
class PageContextFormAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    if (!($entity instanceof PageContextFormInterface)) {
      return AccessResult::forbidden('Unexpected entity type');
    }

    if ($operation === 'edit_paths') {
      $plugin = $entity->getDisplayPlugin('path');

      if ($plugin === NULL) {
        return AccessResult::forbidden()->addCacheableDependency($entity);
      }
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
