<?php

namespace Drupal\a12s_page_context;

/**
 * Interface for the "Page Context Manager" service.
 */
interface PageContextManagerInterface {

  /**
   * Get the list of paths.
   *
   * @return array
   *   The path list.
   */
  public function getPaths(): array;

  /**
   * Get the context data for the current page.
   *
   * @param string $configId
   *   The page context form ID.
   * @param array $context
   *   The plugin context. Default values are calculated by the in-charge
   *   plugins, however it is possible to force some values if desired.
   *
   * @return array
   *   An associative array with page context data.
   */
  public function getData(string $configId, array $context = []): array;

}
