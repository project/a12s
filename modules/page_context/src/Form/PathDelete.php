<?php

namespace Drupal\a12s_page_context\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to delete a page context path.
 */
class PathDelete extends ConfirmFormBase {

  /**
   * The path key.
   *
   * @var string
   */
  protected string $key;

  /**
   * The IDs to remove.
   *
   * @var array
   */
  protected array $ids;

  /**
   * Constructs a new PathDelete object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(protected Connection $connection) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'a12s_page_context_path_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to remove this path?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.a12s_page_context_form.paths_overview');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $path_key
   *   The path key.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $path_key = NULL): array {
    if ($path_key) {
      $this->key = $path_key;
      $this->ids = $this->connection->select('a12s_page_context_record')
        ->fields('a12s_page_context_record', ['id'])
        ->condition('plugin_id', 'path')
        ->condition('key', $path_key)
        ->execute()
        ->fetchCol();

      if ($this->ids) {
        return parent::buildForm($form, $form_state);
      }
    }

    throw new NotFoundHttpException();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->connection->delete('a12s_page_context_record')
      ->condition('id', $this->ids, 'IN')
      ->execute();

    $this->logger('a12s_page_context')->notice('Deleted path settings %key', ['%ip' => $this->key]);
    $this->messenger()->addStatus($this->t('The path was deleted.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
