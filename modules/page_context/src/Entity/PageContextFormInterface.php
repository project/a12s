<?php

namespace Drupal\a12s_page_context\Entity;

use Drupal\a12s_page_context\Plugin\FormDisplayPluginInterface;
use Drupal\a12s_page_context\RecordSet;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface defining a page context form entity type.
 */
interface PageContextFormInterface extends ConfigEntityInterface {

  /**
   * Set the list of enabled themes.
   *
   * @param array $themes
   *   The themes to set.
   */
  public function setThemes(array $themes): void;

  /**
   * Get the display settings.
   *
   * @return array
   *   The display settings.
   */
  public function getDisplaySettings(): array;

  /**
   * Set the display settings.
   *
   * @param array $displaySettings
   *   The display settings.
   */
  public function setDisplaySettings(array $displaySettings): void;

  /**
   * Get the configured display plugin instances.
   *
   * @return array<string, \Drupal\a12s_page_context\Plugin\FormDisplayPluginInterface>
   *   An array whose keys are plugin IDs and values plugin instances.
   */
  public function getDisplayPlugins(): array;

  /**
   * Get the display plugin instance.
   *
   * The provided plugin is already configured for the given ID, if it is
   * defined for the current Page Context form.
   *
   * @param string $pluginId
   *   The expected plugin ID.
   *
   * @return \Drupal\a12s_page_context\Plugin\FormDisplayPluginInterface|null
   *   The display plugin instance, or NULL on failure.
   */
  public function getDisplayPlugin(string $pluginId): ?FormDisplayPluginInterface;

  /**
   * Get the full description.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The full description.
   */
  public function getFullDescription(): MarkupInterface;

  /**
   * Load the stored records for the given page/context.
   *
   * @param array $context
   *   The plugin context.
   *
   * @return \Drupal\a12s_page_context\RecordSet
   *   The record set.
   */
  public function getRecords(array $context = []): RecordSet;

  /**
   * Get the context data for the current page.
   *
   * @param array $context
   *   The plugin context. Default values are calculated by the in-charge
   *   plugins, however it is possible to force some values if desired.
   *
   * @return array
   *   The context data.
   */
  public function getData(array $context = []): array;

  /**
   * Get the form definition as an array.
   *
   * @param \Drupal\a12s_page_context\Plugin\FormDisplayPluginInterface $plugin
   *   The plugin for which the form is built.
   * @param array $context
   *   The plugin context.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array.
   */
  public function getForm(FormDisplayPluginInterface $plugin, array $context, array $form, FormStateInterface $form_state): array;

  /**
   * Alter a form to inject "Page Context Form" elements.
   *
   * @param string $pluginId
   *   The plugin type (theme, path, entity, entity_type...).
   * @param array $context
   *   The plugin context.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $parents
   *   An array of parent keys, defining the location of the element inside the
   *   form.
   */
  public static function alterForm(string $pluginId, array $context, array &$form, FormStateInterface $form_state, array $parents = ['a12s_page_context']): void;

  /**
   * Saves the submitted record to the database.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public static function saveRecord(array $form, FormStateInterface $formState): void;

}
