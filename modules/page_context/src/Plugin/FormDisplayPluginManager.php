<?php

namespace Drupal\a12s_page_context\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * ContextFormDisplay plugin manager.
 */
class FormDisplayPluginManager extends DefaultPluginManager {

  /**
   * Constructs FormDisplayPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/A12sPageContextFormDisplay',
      $namespaces,
      $module_handler,
      'Drupal\a12s_page_context\Plugin\FormDisplayPluginInterface',
      'Drupal\a12s_page_context\Annotation\A12sPageContextFormDisplay'
    );
    $this->alterInfo('a12s_page_context_form_display_info');
    $this->setCacheBackend($cache_backend, 'a12s_page_context_form_display_plugins');
  }

}
