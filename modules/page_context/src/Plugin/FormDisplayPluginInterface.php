<?php

namespace Drupal\a12s_page_context\Plugin;

use Drupal\a12s_page_context\Entity\PageContextFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for a12s_page_context_form_display plugins.
 */
interface FormDisplayPluginInterface extends ConfigurableInterface, PluginFormInterface, PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Whether the plugin applies to the current context or not.
   *
   * @param array $context
   *   The current context.
   *
   * @return bool
   *   TRUE if it applies.
   */
  public function applies(array $context): bool;

  /**
   * Build a context from current page.
   *
   * @param array $context
   *   An array which should contain information required by the plugin.
   *
   * @return array
   *   The current context.
   */
  public function getCurrentContext(array $context = []): array;

  /**
   * Get the key to be used for storage.
   *
   * @param array $context
   *   The current context.
   *
   * @return string|null
   *   The storage key.
   */
  public function getStorageKey(array $context): ?string;

  /**
   * Set the configuration from the input values of a submitted form.
   *
   * @param array $input
   *   The user input.
   */
  public function setConfigurationFromFormInput(array $input): void;

  /**
   * Alter the page context form.
   *
   * @param \Drupal\a12s_page_context\Entity\PageContextFormInterface $pageContextForm
   *   The page context form entity.
   * @param array $context
   *   The current context.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $parents
   *   An array of parent keys, defining the location of the element inside the
   *   form.
   */
  public function alterContextForm(PageContextFormInterface $pageContextForm, array $context, array &$form, FormStateInterface $form_state, array $parents = ['a12s_page_context']): void;

  /**
   * Get the record stored in the database if applicable.
   *
   * @param string $configId
   *   The page context form ID.
   * @param array $context
   *   An array which should contain information required by the plugin.
   *
   * @return array
   *   A record from the database, or an array with default values.
   */
  public function getRecord(string $configId, array $context): array;

  /**
   * Add some query conditions when retrieving records.
   *
   * By default, this will load all entries related to the current plugin, and
   * optionally for the calculated storage key when applicable.
   * But it may be better to add some extra conditions related to current
   * context to avoid loading useless records.
   *
   * @param array $context
   *   The current context.
   *
   * @return \Drupal\Core\Database\Query\Condition|null
   *   The query condition, or NULL if not applies.
   */
  public function addRecordQueryConditions(array $context = []): ?Condition;

  /**
   * Filter the records which match the given context.
   *
   * Context default values should be handled by the plugin.
   *
   * @param \Drupal\a12s_page_context\Record[] $records
   *   The list of records.
   * @param array $context
   *   The current context.
   *
   * @return array
   *   The filtered records.
   */
  public function filterRecords(array $records, array $context): array;

}
