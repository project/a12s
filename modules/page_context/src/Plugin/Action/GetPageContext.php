<?php

namespace Drupal\a12s_page_context\Plugin\Action;

use Drupal\a12s_page_context\Entity\PageContextForm;
use Drupal\a12s_page_context\Entity\PageContextFormInterface;
use Drupal\a12s_page_context\PageContextManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ActionBase;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Calculate the page context and store it into the token environment.
 *
 * @Action(
 *   id = "a12s_page_context_token",
 *   label = @Translation("Page context"),
 *   description = @Translation("Calculate the page context from current scope, and store it as a token."),
 *   type = "a12s_page_context"
 * )
 */
class GetPageContext extends ConfigurableActionBase {

  /**
   * The "Page Context Manager" service.
   *
   * @var \Drupal\a12s_page_context\PageContextManagerInterface
   */
  protected PageContextManagerInterface $contextManager;

  /**
   * The loaded entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected ?EntityInterface $entity;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ActionBase {
    /** @var \Drupal\a12s_page_context\Plugin\Action\GetPageContext $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setPageContextManager($container->get('a12s_page_context.manager'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $config = &$this->configuration;
    $pageContextFormId = &$config['config_id'];
    $data = $pageContextFormId ? $this->contextManager->getData($config['config_id']) : [];

    if (!empty($config['path'])) {
      $parents = explode('][', $config['path']);
      $data = NestedArray::getValue($data, $parents);
    }

    $token = $this->tokenServices;
    $tokenName = isset($config['token_name']) ? trim($config['token_name']) : '';

    if (($tokenName === '') && $pageContextFormId) {
      $tokenName = $pageContextFormId;
    }

    if ($tokenName !== '') {
      $token->addTokenData($tokenName, $data);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $default = [
      'token_name' => '',
      'config_id' => '',
      'path' => '',
    ];

    return $default + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['token_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of token'),
      '#default_value' => $this->configuration['token_name'],
      '#description' => $this->t('Provide the name of a token that holds the loaded page context.'),
      '#weight' => -90,
      '#eca_token_reference' => TRUE,
    ];

    $options = array_map(fn(PageContextFormInterface $contextForm) => $contextForm->label(), PageContextForm::loadMultiple());

    $form['config_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Page context form'),
      '#options' => $options,
      '#default_value' => $this->configuration['config_id'],
      '#description' => $this->t('Select the page context form which the data are extracted.'),
      '#weight' => -70,
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value path'),
      '#default_value' => $this->configuration['path'],
      '#description' => $this->t('The path represents the hierarchy to the value to extract. For example, a valid path can be first_level][my_value. If the path is empty, the whole data are retrieved.'),
      '#weight' => -50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['token_name'] = $form_state->getValue('token_name');
    $this->configuration['config_id'] = $form_state->getValue('config_id');
    $this->configuration['path'] = trim($form_state->getValue('path', ''));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Set the Page Context Manager.
   *
   * @param \Drupal\a12s_page_context\PageContextManagerInterface $contextManager
   *   The entity loader.
   */
  public function setPageContextManager(PageContextManagerInterface $contextManager): void {
    $this->contextManager = $contextManager;
  }

}
