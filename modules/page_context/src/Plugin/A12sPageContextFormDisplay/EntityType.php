<?php

namespace Drupal\a12s_page_context\Plugin\A12sPageContextFormDisplay;

use Drupal\a12s_core\EntityHelperInterface;
use Drupal\a12s_page_context\Entity\PageContextForm;
use Drupal\a12s_page_context\Entity\PageContextFormInterface;
use Drupal\a12s_page_context\Plugin\FormDisplayPluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the a12s_page_context_form.
 *
 * @A12sPageContextFormDisplay(
 *   id = "entity_type",
 *   label = @Translation("Entity type"),
 *   description = @Translation("Display the form on the edit pages of some entity types"),
 * )
 *
 * @noinspection AnnotationMissingUseInspection
 */
class EntityType extends FormDisplayPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\a12s_core\EntityHelperInterface $entityHelper
   *   The Entity Helper service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected EntityTypeManagerInterface $entityTypeManager, protected EntityHelperInterface $entityHelper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('a12s_core.entity_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applies(array $context): bool {
    if (empty($this->configuration['entity_types'])) {
      return FALSE;
    }

    if (empty($context['entity_type']) || empty($context['bundle'])) {
      return FALSE;
    }

    return in_array($context['entity_type'], $this->configuration['entity_types']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentContext(array $context = []): array {
    $context += ['rel' => 'canonical'];

    if (!array_key_exists('entity_type', $context) || !array_key_exists('bundle', $context)) {
      $entity = $context['entity'] ?? $this->entityHelper->getEntityFromRoute($context['rel']);

      if ($entity instanceof EntityInterface) {
        $context += [
          'entity_type' => $entity->getEntityTypeId(),
          'bundle' => $entity->bundle(),
        ];
      }
    }

    return $context + [
      'entity_type' => NULL,
      'bundle' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageKey(array $context): ?string {
    ['entity_type' => $entityType, 'bundle' => $bundle] = $this->getCurrentContext($context);
    return $entityType && $bundle ? "$entityType:$bundle" : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'entity_types' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigurationFromFormInput(array $input): void {
    $raw = $input['entity_types'] ?? [];
    $entityTypes = array_keys(array_filter($raw));
    $this->configuration = ['entity_types' => $entityTypes];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface|SubformStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entityTypeId => $entityType) {
      if ($entityType->entityClassImplements(ContentEntityInterface::class)) {
        $options[$entityTypeId] = $entityType->getLabel();
      }
    }

    if ($options) {
      $form['entity_types'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Entity types'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => $this->configuration['entity_types'] ?? [],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterContextForm(PageContextFormInterface $pageContextForm, array $context, array &$form, FormStateInterface $form_state, array $parents = ['a12s_page_context']): void {
    parent::alterContextForm($pageContextForm, $context, $form, $form_state, $parents);

    if (NestedArray::keyExists($form, $parents)) {
      // For entities, we need to add our submit callback to the submit button.
      $form['actions']['submit']['#submit'][] = [PageContextForm::class, 'saveRecord'];

      if ($context['entity_type'] === 'node') {
        $form['a12s_page_context']['#type'] = 'details';
        $form['a12s_page_context']['#group'] = 'additional_settings';
      }
    }
  }

}
