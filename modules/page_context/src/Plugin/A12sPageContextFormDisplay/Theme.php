<?php

namespace Drupal\a12s_page_context\Plugin\A12sPageContextFormDisplay;

use Drupal\a12s_page_context\Entity\PageContextForm;
use Drupal\a12s_page_context\Entity\PageContextFormInterface;
use Drupal\a12s_page_context\Plugin\FormDisplayPluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the a12s_page_context_form.
 *
 * @A12sPageContextFormDisplay(
 *   id = "theme",
 *   label = @Translation("Theme"),
 *   description = @Translation("Display the form on the theme settings page")
 * )
 *
 * @noinspection AnnotationMissingUseInspection
 */
class Theme extends FormDisplayPluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies(array $context): bool {
    if (empty($context['theme'])) {
      return FALSE;
    }

    if (empty($context['a12s_page_context_form']) || !($context['a12s_page_context_form'] instanceof PageContextFormInterface)) {
      return FALSE;
    }

    // @todo create a method in ContextFormInterface for this.
    if ($context['a12s_page_context_form']->get('all_themes') || in_array($context['theme'], $context['a12s_page_context_form']->get('themes') ?? [])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentContext(array $context = []): array {
    return $context + ['theme' => \Drupal::service('theme.manager')->getActiveTheme()->getName()];
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageKey(array $context): ?string {
    return $context['theme'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function alterContextForm(PageContextFormInterface $pageContextForm, array $context, array &$form, FormStateInterface $form_state, array $parents = ['a12s_page_context']): void {
    parent::alterContextForm($pageContextForm, $context, $form, $form_state, $parents);

    if (NestedArray::keyExists($form, $parents)) {
      $form['#submit'][] = [PageContextForm::class, 'saveRecord'];
    }
  }

}
