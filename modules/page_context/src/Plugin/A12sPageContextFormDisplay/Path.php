<?php

namespace Drupal\a12s_page_context\Plugin\A12sPageContextFormDisplay;

use Drupal\a12s_page_context\Plugin\FormDisplayPluginBase;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of the a12s_page_context_form.
 *
 * @A12sPageContextFormDisplay(
 *   id = "path",
 *   label = @Translation("Path"),
 *   description = @Translation("Display the form on some specific paths")
 * )
 *
 * @noinspection AnnotationMissingUseInspection
 */
class Path extends FormDisplayPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The key used for storage (MD5 hash of settings).
   *
   * @var string|null
   */
  protected ?string $key = NULL;

  /**
   * Constructs a Path plugin.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   An alias manager to find the alias for the current system path.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   The path matcher service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    protected AliasManagerInterface $aliasManager,
    protected PathMatcherInterface $pathMatcher,
    protected RequestStack $requestStack,
    protected CurrentPathStack $currentPath,
    array $configuration,
    string $plugin_id,
    array $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $container->get('path_alias.manager'),
      $container->get('path.matcher'),
      $container->get('request_stack'),
      $container->get('path.current'),
      $configuration,
      $plugin_id,
      $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getStorageKey(array $context): ?string {
    // We don't rely on a storage key for this plugin.
    return $context['path_key'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   *
   * The path plugin may apply to any route.
   */
  public function applies(array $context): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * We no more use the "request_path" condition plugin, as it only relies on
   * the current request and does not allow the caller to specify a specific
   * path in the $context variable.
   *
   * Moreover, it evaluates first the path alias, which leads to load the alias
   * from database or cache, even if not necessary.
   */
  public function filterRecords(array $records, array $context): array {
    return array_filter($records, function ($record) use ($context) {
      if ($record->getPluginId() !== $this->getPluginId()) {
        return TRUE;
      }

      if ($settings = $record->getSettings()) {
        if (!($pages = mb_strtolower($settings['pages']))) {
          $result = TRUE;
        }
        // A NULL value for the "path" key in the $context variable makes this
        // plugin to be ignored.
        elseif (array_key_exists('path', $context) && !isset($context['path'])) {
          return FALSE;
        }
        else {
          if (empty($context['path'])) {
            $request = $this->requestStack->getCurrentRequest();
            $context['path'] = $this->currentPath->getPath($request);
          }

          $context['path'] = $context['path'] === '/' ? $context['path'] : rtrim($context['path'], '/');

          if ($this->pathMatcher->matchPath($context['path'], $pages)) {
            $result = TRUE;
          }
          else {
            $pathAlias = mb_strtolower($this->aliasManager->getAliasByPath($context['path']));

            if ($context['path'] != $pathAlias) {
              $result = $this->pathMatcher->matchPath($pathAlias, $pages);
            }
            else {
              $sourcePath = $this->aliasManager->getPathByAlias($context['path']);
              $result = ($sourcePath !== $context['path']) && $this->pathMatcher->matchPath($sourcePath, $pages);
            }
          }
        }

        return !empty($settings['negate']) ? !$result : $result;
      }

      return FALSE;
    });
  }

}
