<?php

namespace Drupal\a12s_page_context\Plugin;

use Drupal\a12s_page_context\Entity\PageContextFormInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for a12s_page_context_form plugins.
 */
abstract class FormDisplayPluginBase extends PluginBase implements FormDisplayPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function applies(array $context): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentContext(array $context = []): array {
    return $context;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigurationFromFormInput(array $input): void {
    $this->setConfiguration($input);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function alterContextForm(PageContextFormInterface $pageContextForm, array $context, array &$form, FormStateInterface $form_state, array $parents = ['a12s_page_context']): void {
    $context['a12s_page_context_form'] = $pageContextForm;

    if ($this->applies($context)) {
      if (!NestedArray::keyExists($form, $parents)) {
        NestedArray::setValue($form, $parents, [
          '#type' => 'fieldset',
          '#title' => $this->t('Page contexts'),
          '#tree' => TRUE,
        ]);
      }

      $subFormParents = array_merge($parents, [$pageContextForm->id()]);

      $subForm = [
        '#type' => 'details',
        '#title' => $pageContextForm->label(),
        '#open' => TRUE,
        '#description' => $pageContextForm->getFullDescription(),
        '#parents' => $subFormParents,
        '#attached' => [
          'library' => ['a12s_page_context/page-context-form'],
        ],
        'id' => ['#type' => 'value', '#value' => NULL],
        'config_id' => ['#type' => 'value', '#value' => $pageContextForm->id()],
        'plugin_id' => ['#type' => 'value', '#value' => $this->getPluginId()],
        'key' => ['#type' => 'value', '#value' => NULL],
        'data' => [],
      ];

      $subformState = SubformState::createForSubform($subForm, $form, $form_state);
      $elements = $pageContextForm->getForm($this, $context, $subForm, $subformState);
      NestedArray::setValue($form, $subFormParents, $elements);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRecord(string $configId, array $context): array {
    $record = [
      'id' => NULL,
      'config_id' => $configId,
      'plugin_id' => $this->pluginId,
      'key' => NULL,
      'data' => [],
      'settings' => [],
    ];

    if ($key = $this->getStorageKey($context)) {
      $record['key'] = $key;
      $result = \Drupal::database()->select('a12s_page_context_record')
        ->fields('a12s_page_context_record', ['id', 'data', 'settings'])
        ->condition('config_id', $configId)
        ->condition('plugin_id', $this->getPluginId())
        ->condition('key', $key)
        ->execute()
        ->fetchAssoc();

      if ($result) {
        $record['id'] = $result['id'];
        $record['data'] = (!empty($result['data'])) ? unserialize($result['data']) : [];
        $record['settings'] = (!empty($result['settings'])) ? unserialize($result['settings']) : [];
      }
    }

    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function addRecordQueryConditions(array $context = []): ?Condition {
    $context = $this->getCurrentContext($context);

    if ($this->applies($context)) {
      $condition = (\Drupal::database()->condition('AND'))
        ->condition('plugin_id', $this->getPluginId());

      if ($key = $this->getStorageKey($context)) {
        $condition->condition('key', $key);
      }

      return $condition;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function filterRecords(array $records, array $context): array {
    return $records;
  }

}
