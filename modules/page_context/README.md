## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Recommended modules
- Configuration
- Upgrading
- Maintainers


## INTRODUCTION

The A12s Page Context module allows to build forms which defines page contexts.

Each form can be displayed in several places, using plugins, for example the 
theme settings, or an entity edit page.

The final values for a given page depend on the values defined by each enabled 
and compatible plugin. They can be accessed using a service.

```php
/** @var \Drupal\a12s_page_context\PageContextManager $contextManager */
$contextManager = \Drupal::service('a12s_page_context.manager');
// [Optional] A $context variable may force some values of the plugin's
// configuration.
// By default, the default context values are calculated by each plugin, from
// the current request.
$context = ['theme' => 'my_theme'];
$values = $contextManager->getData('my_form_id', $context);
```

## REQUIREMENTS

This module is part of the _A12s Suite_ and requires the `a12s` module..


## INSTALLATION

The installation of this module is like other Drupal modules.

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the a12s module running
   ```composer require "drupal/a12s"```. Otherwise, copy/upload the A12s
   module to the modules directory of your Drupal installation.

2. Enable the 'a12s' and 'a12s_page_context' modules in 'Extend'.
   (`/admin/modules`)

3. Set up user permissions. (`/admin/people/permissions#module-a12s-page-context`)


## CONFIGURATION

- Build a new Page Context Form (`/admin/config/workflow/a12s-page-context`).

- Add the desired plugins, so the form can be fill with expected values:
  - **Theme** allows to define the values from the theme settings
    (`/admin/appearance/settings/my_theme`)
  - **Entity type** allows to define the values in the related 'entity type'
    edit forms (per bundle). In this case, the values are applied on all entity
    pages of the defined entity types.
  - **Entity** allows to define the values for any entity of the defined entity
    types and bundles.


## MAINTAINERS

Current maintainers for Drupal 9/10:

- Fabien Leroux (B-Prod) - https://www.drupal.org/u/b-prod
- Lucas Gratien (fonski) - https://www.drupal.org/u/fonski
- Edison Emond (edison08) - https://www.drupal.org/u/edison08
- Nicolas Losson (nicolas-lsn) - https://www.drupal.org/u/nicolas-lsn

This project has the following Growth Backers:

- Aïon Solutions - https://www.aion-solutions.lu/
