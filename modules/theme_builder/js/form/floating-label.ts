(function(Drupal, once) {

  /**
   * Toggle the "has-value" class on specific form elements.
   */
  const formInputChangeEvent = (e: Event): void => {
    const { target } = e;

    if (target instanceof HTMLInputElement && target.matches('select, input, textarea')) {
      const formElement = target.closest('.floating-label');

      if (formElement !== null) {
        formElement.classList.toggle('has-value', target.value !== "");
      }
    }
  };

  /**
   * Drupal behaviors for form elements with floating label.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.a12sThemeBuilderFormFloatingLabel = {

    /**
     * Attach Drupal behaviors.
     *
     * @param context Element The current execution context
     */
    attach: function (context) {
      if (once('a12sThemeBuilder-form-floating-label', 'html', context).length) {
        document.querySelector('body').addEventListener('change', formInputChangeEvent);
      }
    }
  };

})(window.Drupal, window.once);
