(function(Drupal, Disclosure) {

  'use strict';

  Drupal.behaviors.a12sThemeBuilderDisclosureAccordion = {

    attach: function (context) {
      // @todo add settings form and get options from drupalSettings.
      Disclosure.init({
        parentSelector: ".accordion",
        toggleSelector: ".accordion-toggle",
        collapseOthers: true,
        context : context,
      });
    }

  };

})(window.Drupal, window.A12s.Disclosure);
