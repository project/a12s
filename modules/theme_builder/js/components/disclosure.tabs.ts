(function(Drupal, Disclosure) {

  'use strict';

  Drupal.behaviors.a12sThemeBuilderDisclosureTabs = {

    attach: function (context) {
      // @todo add settings form and get options from drupalSettings.
      Disclosure.init({
        parentSelector: ".tabs",
        toggleSelector: ".tab",
        ariaAttribute: "aria-selected",
        collapseOthers: true,
        atLeastOneExpanded: true,
        context : context,
      });
    }

  };

})(window.Drupal, window.A12s.Disclosure);
