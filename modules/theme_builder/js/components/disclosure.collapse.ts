(function(Drupal, Disclosure) {

  'use strict';

  Drupal.behaviors.a12sThemeBuilderDisclosureCollapse = {

    attach: function (context) {
      // @todo add settings form and get options from drupalSettings.
      Disclosure.init({context: context});
    }
  };

})(window.Drupal, window.A12s.Disclosure);
