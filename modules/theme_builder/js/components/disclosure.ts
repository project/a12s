export interface DisclosureEventParams {
  toggle: HTMLElement,
  disclosureInstance: Disclosure,
}

// @todo should we use 4 different events, or only 1 event with a "state" property?
interface CustomEventMap {
  "disclosureContent.expand": CustomEvent<DisclosureEventParams>;
  "disclosureContent.expanded": CustomEvent<DisclosureEventParams>;
  "disclosureContent.collapse": CustomEvent<DisclosureEventParams>;
  "disclosureContent.collapsed": CustomEvent<DisclosureEventParams>;
}

export interface DisclosureInitOptions {
  parentSelector?: string,
  toggleSelector: string,
  context: Document|Element,
  ariaAttribute: string,
  collapseOthers: boolean;
  atLeastOneExpanded: boolean;
  group?: string;
}

export interface DisclosureSettings {
  ariaAttribute: string,
  collapseOthers: boolean;
  atLeastOneExpanded: boolean;
  group?: string;
}

interface DisclosureInstanceState {
  expanded?: boolean,
  group?: string,
}

declare global {
  interface Document {
    addEventListener<K extends keyof CustomEventMap>(type: K, listener: (this: Document, ev: CustomEventMap[K]) => void): void;
    dispatchEvent<K extends keyof CustomEventMap>(ev: CustomEventMap[K]): void;
  }
}

function randomGroupName(): string {
  return Math.floor(Math.random() * Date.now()).toString(36);
}

function moveChildren(source: Element, target: Element) {
  while (source.childNodes.length > 0) {
    target.appendChild(source.childNodes[0]);
  }
}

export default class Disclosure {

  static readonly EXPANDED_CLASS = "show";

  static readonly DEFAULT_SELECTOR = ".disclosure";

  static readonly DEFAULT_ARIA_ATTRIBUTE = "aria-expanded";

  element: HTMLElement;

  targets: Map<string, DisclosureInstanceState>;

  protected isWrapped: boolean;

  settings: DisclosureSettings;

  eventListener: EventListener|null = null;

  transitioning = false;

  protected static instances: Map<HTMLElement, Disclosure> = new Map();

  protected constructor(element: HTMLElement, settings: Partial<DisclosureSettings>) {
    this.element = element;
    this.settings = {...{ariaAttribute: Disclosure.DEFAULT_ARIA_ATTRIBUTE, collapseOthers: false, atLeastOneExpanded: false}, ...settings};

    const controls: ([string, any])[] = [];

    this.isWrapped = !('wrap' in this.element.dataset && this.element.dataset.wrap === 'false');

    if ('target' in this.element.dataset && this.element.dataset.target !== '') {
      this.element.dataset.target.split(',').forEach((target) => {
        target = target.trim();
        if (target !== '') {
          controls.push([target, {}]);
        }
      });
    }

    if (!controls.length) {
      this.element.getAttribute("aria-controls")?.split(/\s*,\s*/).forEach((target) => {
        target = target.trim();
        if (target !== '') {
          controls.push(["#" + target, {}]);
        }
      });
    }

    this.targets = new Map(controls);
    this.attach();
  }

  attach(): void {
    if (this.eventListener === null && this.targets?.size) {
      this.getTargetElements().forEach((target: HTMLElement) => {
        const contentIsExpanded = target.classList.contains(Disclosure.EXPANDED_CLASS);
        this.setExpandedState(target, contentIsExpanded);

        if (this.isWrapped) {
          target.classList.add("disclosure-panel");
          // Wrap the content, to be compatible with our CSS "grid" behavior.
          const innerWrapper = document.createElement('div');
          innerWrapper.classList.add('disclosure-panel-inner');
          const contentWrapper = document.createElement('div');
          contentWrapper.classList.add('disclosure-panel-content');
          moveChildren(target, contentWrapper);
          innerWrapper.insertAdjacentElement('afterbegin', contentWrapper);
          target.insertAdjacentElement('afterbegin', innerWrapper);
        }
        else {
          target.classList.add("disclosure-target");
        }

        this.toggleContent(target, this.isExpanded());
      });

      this.eventListener = this.onClick.bind(this);
      this.element.addEventListener("click", this.eventListener);
    }
  }

  setExpandedState(element: HTMLElement, expanded: boolean) {
    if (element.id) {
      const id = "#" + element.id;

      if (this.targets.has(id)) {
        const state = this.targets.get(id);
        state.expanded = expanded;
        this.targets.set(id, state);
      }
    }
  }

  getGroup(): string|null {
    return "group" in this.settings && this.settings.group !== undefined ? this.settings.group : null;
  }

  getOthers(): Array<Disclosure> {
    const group = this.getGroup();

    if (group) {
      return [...Disclosure.instances.values()].filter((instance: Disclosure) => instance.getGroup() === group && instance !== this );
    }

    return [];
  }

  isExpanded(): boolean {
    return this.element.getAttribute(this.settings.ariaAttribute) === "true";
  }

  getTargetElements(): HTMLElement[] {
    const list: HTMLElement[] = [];
    const targets = [...this.targets.keys()].filter((target) => {
      if (target === 'sibling') {
        if (this.element.nextElementSibling instanceof HTMLElement) {
          list.push(this.element.nextElementSibling);
        }
        return false;
      }

      return true;
    });

    document.querySelectorAll([...this.targets.keys()].join(",")).forEach((element) => {
      if (element instanceof HTMLElement) {
        list.push(element);
      }
    });

    return list;
  }

  toggle(): void {
    if (!this.transitioning) {
      const isExpanded = this.isExpanded();

      if (isExpanded && this.settings.atLeastOneExpanded) {
        if (this.getOthers().find((instance) => instance.isExpanded()) === undefined) {
          return;
        }
      }

      if (this.settings.collapseOthers && !isExpanded) {
        this.getOthers().forEach((instance) => {
          if (instance.isExpanded()) {
            instance.getTargetElements().forEach((target: HTMLElement) => {
              this.toggleContent(target, false);
            });

            instance.element.setAttribute(this.settings.ariaAttribute, "false");
          }
        });
      }

      this.transitioning = true;
      const promises: Array<Promise<void>> = [];
      const eventInfo = {
        detail: {
          toggle: this.element,
          disclosureInstance: this,
        }
      };

      this.element.dispatchEvent(new CustomEvent(isExpanded ? "disclosureContent.collapse" : "disclosureContent.expand", eventInfo));

      this.getTargetElements().forEach((target: HTMLElement) => {
        promises.push(new Promise((resolve) => {
          // Does the target element have transition?
          const style = window.getComputedStyle(target, null);
          const transitionDuration = Math.max(
            parseFloat(style.getPropertyValue("animation-duration") || "0"),
            parseFloat(style.getPropertyValue("transition-duration") || "0")
          );

          this.toggleContent(target, !isExpanded);

          if (transitionDuration > 0) {
            // We use setTimeout to avoid issues with transitions being cancelled.
            setTimeout(() => { resolve(); }, transitionDuration * 1000);
          }
          else {
            resolve();
          }
        }));
      });

      this.element.setAttribute(this.settings.ariaAttribute, !isExpanded ? "true" : "false");

      Promise.all(promises).then(() => {
        // Update ARIA state if any toggle with same targets.
        Disclosure.instances.forEach((instance, element) => {
          if (instance !== this) {
            let targetMatch = false;

            if (this.targets.size > 0 && this.targets.size === instance.targets?.size) {
              targetMatch = [...this.targets.keys()].every((key) => instance.targets.has(key));
            }

            if (targetMatch) {
              instance.element.setAttribute(instance.settings.ariaAttribute, !isExpanded ? "true" : "false");
            }
          }
        });

        this.element.dispatchEvent(new CustomEvent(isExpanded ? "disclosureContent.collapsed" : "disclosureContent.expanded", eventInfo));
        this.transitioning = false;
      });
    }
  }

  toggleContent(target: HTMLElement, expand: boolean): void {
    target.classList.toggle(Disclosure.EXPANDED_CLASS, expand);
    this.setExpandedState(target, expand);
    target.setAttribute("aria-hidden", expand ? "false" : "true");
  }

  onClick(e: PointerEvent): void {
    e.preventDefault();
    this.toggle();
  }

  destroy(): void {
    if (this.eventListener !== null) {
      this.element.removeEventListener("click", this.eventListener);
      this.eventListener = null;
    }

    // Remove the wrappers.
    this.getTargetElements().forEach((target: HTMLElement) => {
      const innerWrapper = target.querySelector(':scope > .disclosure-panel-inner');

      if (innerWrapper) {
        const contentWrapper = innerWrapper.querySelector(':scope > .disclosure-panel-content');

        if (contentWrapper) {
          moveChildren(contentWrapper, target);
        }

        innerWrapper.remove();
      }
    });

    if (Disclosure.instances.has(this.element)) {
      Disclosure.instances.delete(this.element);
    }
  }

  public static init(options: Partial<DisclosureInitOptions> = {}) {
    const defaultOptions: DisclosureInitOptions = {
      toggleSelector: Disclosure.DEFAULT_SELECTOR,
      context: document,
      ariaAttribute: Disclosure.DEFAULT_ARIA_ATTRIBUTE,
      collapseOthers: false,
      atLeastOneExpanded: false,
    };

    options = {...defaultOptions, ...options};

    if (!(options.context instanceof Element || options.context instanceof Document)) {
      return;
    }

    if ("group" in options && (!(typeof options.group === 'string') || options.group.length === 0)) {
      delete options.group;
    }

    if (options.parentSelector !== undefined) {
      options.context.querySelectorAll(options.parentSelector).forEach((parent: HTMLElement) => {
        const childOptions = { ...options };
        delete childOptions.parentSelector;

        // Having a parent means that we are dealing with a group, so we need to ensure a group is defined.
        if (!("group" in childOptions)) {
          childOptions.group = "disclosureGroup" in parent.dataset && parent.dataset.tabsGroup.length > 0 ? parent.dataset.tabsGroup : randomGroupName();
        }

        Disclosure.init(childOptions);
      });
    }
    else {
      const settings: Partial<DisclosureSettings> = {
        ariaAttribute: options.ariaAttribute,
        collapseOthers: options.collapseOthers,
        atLeastOneExpanded: options.atLeastOneExpanded,
        group: "group" in options ? options.group : undefined
      };

      options.context.querySelectorAll(options.toggleSelector).forEach((toggle: HTMLElement) => {
        Disclosure.addInstance(toggle, settings);
      });
    }
  }

  public static addInstance(element: HTMLElement, settings: Partial<DisclosureSettings> = {}): Disclosure {
    if (!Disclosure.instances.has(element)) {
      Disclosure.instances.set(element, new Disclosure(element, settings));
    }

    return Disclosure.instances.get(element);
  }

  public static getInstance(element: HTMLElement): Disclosure|undefined {
    return Disclosure.instances.get(element);
  }

  public static deleteInstance(element: HTMLElement): void {
    Disclosure.getInstance(element)?.destroy();
  }

}
