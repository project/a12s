<?php

namespace Drupal\a12s_theme_builder;

use Drupal\a12s_core\FormHelperTrait;
use Drupal\a12s_core\ThemeHelperTrait;
use Drupal\block\Entity\Block;
use Drupal\Component\Utility\NestedArray;

/**
 * The "Theme Helper" service.
 */
class ThemeHelper implements ThemeHelperInterface {

  use FormHelperTrait {
    FormHelperTrait::getInputNameFromPath as public;
    FormHelperTrait::getInputSelectorFromPath as public;
  }
  use ThemeHelperTrait {
    ThemeHelperTrait::addClasses as public;
    ThemeHelperTrait::setAttribute as public;
  }

  /**
   * {@inheritDoc}
   */
  public function getBlockRegion(array $variables): ?string {
    $region = NULL;

    if (!empty($variables['elements']['#id'])) {
      $region = Block::load($variables['elements']['#id'])?->getRegion();
    }
    // Try to find region when using page_manager module.
    elseif (isset($variables['elements']['#configuration']['region'])) {
      $region = $variables['elements']['#configuration']['region'];
    }

    return $region;
  }

  /**
   * {@inheritDoc}
   */
  public function injectSuggestions(array &$suggestions, string $base_hook, string $injectName, string $injectValue, array $variables): void {
    $base_suggestions = $suggestions;
    $suggestions = [$base_hook . '__' . $injectName . '_' . $injectValue];

    foreach ($base_suggestions as $suggestion) {
      if (!in_array($suggestion, $suggestions)) {
        $suggestions[] = $suggestion;
      }

      $match = [];
      // Insert just after the base hook.
      if (preg_match('/^(?P<start>' . preg_quote($base_hook, '/') . ')(?P<end>__.*)$/', $suggestion, $match)) {
        $suggestions[] = $match['start'] . '__' . $injectName . '_' . $injectValue . $match['end'];
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getThemeSettingValue(array $parents, string $default = NULL): mixed {
    $value = NULL;
    $property = array_shift($parents);

    if ($property && ($settings = theme_get_setting($property))) {
      if ($parents) {
        if (is_array($settings)) {
          $value = NestedArray::getValue($settings, $parents);
        }
      }
      else {
        $value = $settings;
      }

      if ($value === NULL && $default !== NULL) {
        $value = $default;
      }
    }

    return $value;
  }

}
