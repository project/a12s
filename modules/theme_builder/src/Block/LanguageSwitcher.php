<?php

declare(strict_types=1);

namespace Drupal\a12s_theme_builder\Block;

use Drupal\block\BlockInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Provides options to configure the display of the language switcher block.
 */
class LanguageSwitcher extends SettingsAlterBase {

  /**
   * {@inheritdoc}
   */
  public function defaultValues(): array {
    return [
      'display' => 'list',
      'use_langcode' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function applies(BlockInterface $entity): bool {
    return str_starts_with($entity->getPluginId(), 'language_block:');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationKey(): string {
    return 'language_switcher';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, SubformStateInterface $formState, array $settings = []): void {
    $this->mergeWithDefault($settings);
    $form['display'] = [
      '#type' => 'select',
      '#title' => $this->t('Display style'),
      '#options' => [
        'dropdown' => $this->t('Dropdown'),
        'list' => $this->t('Bullet list'),
      ],
      '#default_value' => $settings['display'],
    ];

    $form['use_langcode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the language code instead of the name'),
      '#default_value' => $settings['use_langcode'],
    ];
  }

  /**
   * Preprocess a "language switcher" block.
   *
   * @see a12s_theme_builder_preprocess_block()
   */
  public function preprocessBlock(array &$variables, array $settings): void {
    $this->mergeWithDefault($settings);

    $languageType = $variables['derivative_plugin_id'] ?? LanguageInterface::TYPE_INTERFACE;
    $currentLanguage = \Drupal::languageManager()->getCurrentLanguage($languageType);
    $activeLanguageId = $currentLanguage->getId();
    $variables['use_dropdown'] = $settings['display'] === 'dropdown';

    if (!empty($variables['content']['#links'])) {
      foreach ($variables['content']['#links'] as $key => &$link) {
        // Remove the active language from the list when using a dropdown display.
        if ($variables['use_dropdown'] && $key === $activeLanguageId) {
          unset($variables['content']['#links'][$key]);
          continue;
        }

        // Use language ID as title instead of names.
        if ($settings['use_langcode']) {
          $link['title'] = $key;
        }
      }

      if ($variables['use_dropdown']) {
        $id = $variables['attributes']['id'] ?? 'block-' . HTML::cleanCssIdentifier($variables['plugin_id']);
        $id = Html::getUniqueId($id . '-language-switcher-dropdown');

        $variables['content']['#heading'] = [
          'text' => $settings['use_langcode'] ? $activeLanguageId : $currentLanguage->getName(),
          'level' => 'div',
          'attributes' => [
            'class' => ['disclosure'],
            'id' => $id . '-label',
            'aria-controls' => $id,
            'aria-expanded' => 'false',
            'type' => 'button',
          ],
        ];

        $variables['#attached']['library'][] = 'a12s_theme_builder/language-switcher-dropdown';
        $variables['attributes']['class'][] = 'block-language-switcher-dropdown';
        $variables['content']['#attributes']['id'] = $id;
        $variables['content']['#attributes']['aria-hidden'] = 'true';
        $variables['content']['#attributes']['aria-labelledby'] = $id . '-label';
      }
    }
  }

}
