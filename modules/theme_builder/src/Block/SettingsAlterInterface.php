<?php

declare(strict_types=1);

namespace Drupal\a12s_theme_builder\Block;

use Drupal\block\BlockInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Defines methods to alter block settings forms.
 */
interface SettingsAlterInterface extends TrustedCallbackInterface {

  /**
   * Get an instance of the current class.
   *
   * @return static|null
   */
  public static function getInstance(): ?static;

  /**
   * Alter the block settings form, if applicable.
   *
   * @param \Drupal\block\BlockInterface $entity
   *   The block to alter.
   * @param array $form
   *   The block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The block configuration form state.
   *
   * @return void
   */
  public static function alterForm(BlockInterface $entity, array &$form, FormStateInterface $formState): void;

  /**
   * Submit callback for the alter subform.
   *
   * @param array $form
   *   The block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The block configuration form state.
   */
  public static function submit(array $form, FormStateInterface $formState): void;

  /**
   * Alters the render array for a block.
   *
   * @param \Drupal\block\BlockInterface $block
   *   The block object.
   * @param array $build
   *   The render array for the block.
   *
   * @see hook_form_FORM_ID_alter()
   * @see \Drupal\a12s_theme_builder\Block\SettingsAlterInterface::preRenderBlock()
   */
  public static function alterView(BlockInterface $block, array &$build): void;

  /**
   * Get the third-party setting name.
   *
   * @return string
   */
  public function configurationKey(): string;

  /**
   * Whether the current instance should process the block entity or not.
   *
   * @param \Drupal\block\BlockInterface $entity
   *   The block entity.
   *
   * @return bool
   */
  public function applies(BlockInterface $entity): bool;

  /**
   * Get the default values.
   *
   * @return array
   *   An associative array containing the default values.
   */
  public function defaultValues(): array;

  /**
   * Build the block settings subform.
   *
   * @param array $form
   *   A renderable array representing the form.
   * @param \Drupal\Core\Form\SubformStateInterface $formState
   *   The current state of the form.
   * @param array $settings
   *   The stored settings.
   */
  public function buildForm(array &$form, SubformStateInterface $formState, array $settings = []): void;

  /**
   * Submit callback; save the third party settings.
   *
   * @param array $form
   *   The form array being submitted.
   * @param \Drupal\Core\Form\SubformStateInterface $formState
   *   The current state of the form.
   */
  public function submitForm(array &$form, SubformStateInterface $formState): void;

  /**
   * #pre_render callback for building a block.
   */
  public function preRenderBlock($build): array;

}
