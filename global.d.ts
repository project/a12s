import drupal from "typed-drupal-core/source/drupal.core.drupal";
import * as drupalAjax from "typed-drupal-core/source/drupal.core.ajax";
import * as drupalDialog from "typed-drupal-core/source/drupal.core.dialog";

declare global {

  namespace drupal {

    namespace Core {

      export interface IBehaviors {
        [key: string]: IBehavior,
      }

    }

    // Do not use definition in "typed-drupal-core/source/drupal.core.debounce", as it is false.
    export interface IDrupalStatic {

      debounce?(
        func: (...args: any[]) => any,
        wait?: number,
        immediate?: boolean
      ): Function;

    }

  }

  interface Window {

    once(id: string, selector: NodeList|Array<Element>|Element|string, context: Document|DocumentFragment|Element): Array<HTMLElement>;

  }

}

export type {}
