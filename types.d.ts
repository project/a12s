import Disclosure from "./modules/theme_builder/js/components/disclosure";

interface A12s {

  Disclosure?: typeof Disclosure;

}

declare global {
  interface Window {

    A12s?: A12s;

  }

}
