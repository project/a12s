const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

const config = [];

function standaloneLibrary(src, subPath, defaultExport = false, externals = null) {
  const baseMapConfig = {
    entry: {},
    output: {
      path: path.resolve(__dirname),
      filename: '[name].js',
      library: ['A12s'],
      libraryTarget: 'umd',
      libraryExport: 'default',
    },
    optimization: {
      minimize: (process.env.NODE_ENV === 'production'),
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.ts$/,
          exclude: /node_modules/,
          use: ['babel-loader', 'ts-loader']
        }
      ]
    },
    resolve: {
      extensions: ['.ts', '.js']
    },
  };

  baseMapConfig.entry[src] = path.resolve(__dirname, './' + src + '.ts');
  baseMapConfig.output.library = baseMapConfig.output.library.concat(subPath);

  if (defaultExport) {
    baseMapConfig.output.libraryExport = 'default';
  }

  if (externals) {
    baseMapConfig.externals = externals;
  }

  config.push(baseMapConfig);
}

standaloneLibrary('modules/theme_builder/js/components/disclosure', ['Disclosure'], true);

config.push({
  entry: {
    'modules/theme_builder/js/components/disclosure.accordion': path.resolve(__dirname, './modules/theme_builder/js/components/disclosure.accordion.ts'),
    'modules/theme_builder/js/components/disclosure.collapse': path.resolve(__dirname, './modules/theme_builder/js/components/disclosure.collapse.ts'),
    'modules/theme_builder/js/components/disclosure.tabs': path.resolve(__dirname, './modules/theme_builder/js/components/disclosure.tabs.ts'),
    'modules/theme_builder/js/form/floating-label': path.resolve(__dirname, './modules/theme_builder/js/form/floating-label.ts'),
  },
  output: {
    path: path.resolve(__dirname),
    filename: '[name].js'
  },
  optimization: {
    minimize: (process.env.NODE_ENV === 'production'),
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          mangle: {
            reserved: ['Drupal']
          }
        }
      })
    ]
  },
  externals: {
    'once': 'window.once',
    'drupalSettings': 'window.drupalSettings',
    'Drupal': 'window.Drupal',
    'DrupalMap': 'window.DrupalMap',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
});

module.exports = config;
