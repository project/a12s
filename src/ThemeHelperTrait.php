<?php

declare(strict_types=1);

namespace Drupal\a12s_core;

use Drupal\Core\Template\Attribute;

trait ThemeHelperTrait {

  /**
   * Add the specified classes to the given attribute instance.
   *
   * @todo Add support for array of classes.
   */
  protected function addClasses(Attribute|array &$attributes, string $value): void {
    foreach (preg_split('/\s+/', $value) as $class) {
      if ($attributes instanceof Attribute) {
        $attributes->addClass($class);
      }
      else {
        $attributes += ['class' => []];

        if (!in_array($class, $attributes['class'])) {
          $attributes['class'][] = $class;
        }
      }
    }
  }

  /**
   * Set an attribute with given name to either an attribute instance or array.
   *
   * @param \Drupal\Core\Template\Attribute|array $attributes
   * @param string $name
   * @param string $value
   */
  protected function setAttribute(Attribute|array &$attributes, string $name, string $value = ''): void {
    if ($attributes instanceof Attribute) {
      $attributes->setAttribute($name, $value);
    }
    else {
      $attributes[$name] = $value;
    }
  }

}
