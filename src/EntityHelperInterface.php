<?php

namespace Drupal\a12s_core;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Interface for the Entity Helper service.
 */
interface EntityHelperInterface {

  /**
   * Creates a new EntityHelper instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RouteMatchInterface $route_match);

  /**
   * Get an entity field if it exists and is not empty.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity instance.
   * @param string $fieldName
   *   The entity reference field name.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field item list, containing the field items.
   */
  public function entityGetField(FieldableEntityInterface $entity, string $fieldName): ?FieldItemListInterface;

  /**
   * Get a referenced entity.
   *
   * Try to find an entity from an entity reference field of the given entity,
   * if it exists and is not null.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity instance.
   * @param string $fieldName
   *   The entity reference field name.
   * @param int $delta
   *   The delta of the field reference item.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The referenced entity or NULL.
   */
  public function entityGetFieldReferenceEntity(FieldableEntityInterface $entity, string $fieldName, int $delta = 0): ?EntityInterface;

  /**
   * Get a list of referenced entities.
   *
   * Try to find a field item list from an entity reference field of the given
   * entity, if it exists and is not null.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity instance.
   * @param string $erFieldName
   *   The entity reference field name.
   * @param string $fieldName
   *   The field name.
   * @param int $erDelta
   *   The delta of the field reference item.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field item list containing the referenced entities.
   */
  public function entityGetFieldReferenceSubField(FieldableEntityInterface $entity, string $erFieldName, string $fieldName, int $erDelta = 0): ?FieldItemListInterface;

  /**
   * Try to get the value from the given field of the entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity instance.
   * @param string $fieldName
   *   The field name.
   * @param int $delta
   *   (optional) The delta of the field reference item. Default to 0 (first
   *   item).
   * @param string|null $property
   *   (optional) A key identifier of the value array.
   *
   * @return mixed
   *   The field value or NULL.
   */
  public function entityGetFieldValue(FieldableEntityInterface $entity, string $fieldName, int $delta = 0, string $property = NULL): mixed;

  /**
   * Builds the render array for the provided entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to render.
   * @param string $viewMode
   *   (optional) The view mode that should be used to render the entity.
   * @param ?string $langCode
   *   (optional) For which language the entity should be rendered, defaults to
   *   the current content language.
   *
   * @return array
   *   A render array for the entity.
   */
  public function renderEntity(EntityInterface $entity, string $viewMode = 'full', ?string $langCode = NULL): array;

  /**
   * Check whether the given view mode is enabled.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to render.
   * @param string $viewMode
   *   (optional) The view mode that should be used to render the entity.
   *
   * @return bool
   * @see self::renderEntity()
   */
  public function isViewModeEnabled(EntityInterface $entity, string $viewMode = 'full'): bool;

  /**
   * Get a rendered entity whether the view mode is enabled.
   *
   * It builds the render array for the provided entity, only if the view mode
   * is explicitly enabled.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to render.
   * @param string $viewMode
   *   (optional) The view mode that should be used to render the entity.
   * @param ?string $langCode
   *   (optional) For which language the entity should be rendered, defaults to
   *   the current content language.
   *
   * @return array
   *   A render array.
   *
   * @see self::renderEntity()
   */
  public function renderEntityIfViewModeEnabled(EntityInterface $entity, string $viewMode = 'full', ?string $langCode = NULL): array;

  /**
   * Try to load an entity view display.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $viewMode
   *   (optional) The view mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface|\Drupal\Core\Entity\EntityInterface|null
   *   The view display instance.
   */
  public function loadEntityViewDisplay(string $entityType, string $bundle, string $viewMode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): EntityViewDisplayInterface|EntityInterface|null;

  /**
   * Get displayed entity from the current route.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase|null
   *   Either an entity or FALSE if the current route is not a canonical
   *   entity page.
   */
  public function getEntityFromRoute(?string $rel = 'canonical'): ?ContentEntityBase;

  /**
   * Get the entity type from a route name, if applicable.
   *
   * @param string $routeName
   *   The route name.
   * @param string|null $rel
   *   The link relationship type, for example: canonical or edit-form.
   *
   * @return string|null
   *   The entity type.
   */
  public function getEntityTypeFromRoute(string $routeName, ?string $rel = 'canonical'): ?string;

}
